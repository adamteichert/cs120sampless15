#include <list>
#include <cassert>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cmath>
#include <iomanip>

using std::list;
using std::string;
using std::cout;
using std::cin;
using std::ostream;
using std::istream;
using std::ifstream;
using std::ofstream;
using std::endl;

template <class T>
ostream& operator <<(ostream& os, const list<T>& toWrite) {
    typedef typename list<T>::const_iterator itr_t;
    itr_t itr = toWrite.begin();
    while (itr != toWrite.end()) {
        os << *(itr++);
        // don't put the space on the last one
        if (itr != toWrite.end()) os << " ";
    }
    return os;
}

template <class T>
istream& operator >>(istream& is, list<T>& readInto) {
    T cur;
    while (is >> cur) {
        readInto.push_back(cur);
    }
    return is;
}

void partE() {
    // PART E: try out our reading and writing with files
    cout << "PART E:" << endl;
    // 1) write the list to a file
    list<int> outList = { 1, 2, 3, 4 };
    ofstream outFile("test.txt", std::ios::out);
    outFile << outList;
    outFile.close();
    
    // 2) read the list back in a check for match
    list<int> inList;
    ifstream inFile("test.txt");
    inFile >> inList;
    inFile.close();
    assert(outList == inList);

    // 3) overwrite the file
    outFile.open("test.txt", std::ios::out);
    outFile << outList;
    outFile.close();

    // 4) now open in append mode; append to the end of the file
    // (no whitespace at the end so there will be a 41 in the
    // result)
    outFile.open("test.txt", std::ios::app);
    outFile << outList;
    outFile.close();

    list<int> list1;
    list<int> list2;
    inFile.open("test.txt");
    inFile >> list1 >> list2;
    inFile.close();
    list<int> expectedList1 = { 1, 2, 3, 41, 2, 3, 4 };
    list<int> expectedList2 = { };
    
    assert(list1 == expectedList1);
    assert(list2 == expectedList2);
    cout << "passed." << endl;
}

void partF() {
    // PART F: do some formatted output
    // format some output
    string expected = 
        "********\n"
        "   3.142\n"
        "3.142   \n"
        "3.142...\n"
        "....3.14\n"
        "....3.14\n"
        "********\n";
    std::ostringstream output;
    double pi = M_PI;
    int w = 8;
    output.precision(4);
    output << std::setfill('*') << std::setw(w) << "" << std::setfill(' ') << endl;
    output << std::setw(w) << pi << endl << std::setiosflags(std::ios::left);
    output << std::setw(w) << pi << endl << std::setfill('.');
    output << std::setw(w) << pi << endl << std::setprecision(3) << std::setiosflags(std::ios::right);
    output << std::setw(w) << pi << endl;
    output << std::setw(w) << pi << endl;
    output << std::setfill('*') << std::setw(w) << "" << std::setfill(' ') << endl;
    string found = output.str();
    assert(found == expected);
}

int main() {
    {   // PART A: write a single (non-templated) operator function so
        // that the following will run and pass the assertion:
        cout << "PART A:" << endl;
        list<string> myList = { "s1", "s2", "s3", "s4" };
        cout << myList << endl;
        std::ostringstream oss;
        oss << myList << " " << myList;
        assert(oss.str() == "s1 s2 s3 s4 s1 s2 s3 s4");
        cout << myList << endl;
        cout << "passed." << endl;
    }
    {   // PART B: write a single (non-templated) operator function so
        // that the following will run and pass the assertions:
        cout << "PART B:" << endl;
        list<string> list1;
        list<string> list2;
        std::istringstream iss("s1 s2 s3 s4 s1 s2 s3 s4");
        iss >> list1 >>  list2;
    
        list<string> expectedList1 = { "s1", "s2", "s3", "s4", "s1", "s2", "s3", "s4" };
        list<string> expectedList2 = { };
        
        assert(list1 == expectedList1);
        assert(list2 == expectedList2);
        cout << "passed." << endl;
    }
    {   // PART C: Make your << operator templated so that it can pass
        // the following assertion while still passing PART A
        cout << "PART C:" << endl;
        list<int> myList = { 1, 2, 3, 4 };
        cout << myList << endl;
        std::ostringstream oss;
        oss << myList << " " << myList;
        assert(oss.str() == "1 2 3 4 1 2 3 4");
        cout << myList << endl;
        cout << "passed." << endl;
    }
    {   // PART D: Make your << operator templated so that it can pass
        // the following assertion while still passing PART B
        cout << "PART D:" << endl;
        list<int> list1;
        list<int> list2;
        std::istringstream iss("1 2 3 4 1 2 3 4");
        iss >> list1 >>  list2;
    
        list<int> expectedList1 = { 1, 2, 3, 4, 1, 2, 3, 4 };
        list<int> expectedList2 = { };
        
        assert(list1 == expectedList1);
        assert(list2 == expectedList2);
        cout << "passed." << endl;
    }
    {   // PART E: use file streams instead
        // DO TOGETHER:
        partE();
    }
    {   // PART F: do some formatted output format some output
        // DO TOGETHER:
        partF();
    }
    cout << "All tests passed." << endl;
    #if 0 // COMMENTING OUT FROM HERE TO THE END
    #endif // END OF COMMENTED OUT CODE
}

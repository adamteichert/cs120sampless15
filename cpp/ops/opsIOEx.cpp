#include <list>
#include <cassert>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cmath>
#include <iomanip>

using std::list;
using std::string;
using std::cout;
using std::cin;
using std::ostream;
using std::istream;
using std::ifstream;
using std::ofstream;
using std::endl;

template <class T>
ostream& operator <<(ostream& os, const list<T>& toWrite) {
    typedef typename list<T>::const_iterator itr_t;
    itr_t itr = toWrite.begin();
    while (itr != toWrite.end()) {
	os << *(itr++);
	if (itr != toWrite.end()) os << " ";
    }
    return os;
}

template <class T>
istream& operator >>(istream& is, list<T>& readInto) {
    T cur;
    while (is >> cur) {
	readInto.push_back(cur);
    }
    return is;
}

int main() {
    // INSTRUCTIONS: move the "#if 0" line down in the file as you are
    // ready to work on subsequent parts

    {   // PART A: write a single (non-templated) operator function so
        // that the following will run and pass the assertion:
	//  << list<string>
        cout << "PART A:" << endl;
        list<string> myList = { "s1", "s2", "s3", "s4" };
        cout << myList << endl;
        std::ostringstream oss;
        oss << myList << " " << myList;
        assert(oss.str() == "s1 s2 s3 s4 s1 s2 s3 s4");
        cout << myList << endl;
        cout << "passed." << endl;
    }
    {   // PART B: write a single (non-templated) operator function so
        // that the following will run and pass the assertions:
        cout << "PART B:" << endl;
        list<string> list1;
        list<string> list2;
        std::istringstream iss("s1 s2 s3 s4 s1 s2 s3 s4");
        iss >> list1 >> list2;
    
        list<string> expectedList1 = { "s1", "s2", "s3", "s4", "s1", "s2", "s3", "s4" };
        list<string> expectedList2 = { };
        
        assert(list1 == expectedList1);
        assert(list2 == expectedList2);
        cout << "passed." << endl;
    }
    {   // PART C: Make your << operator templated so that it can pass
        // the following assertion while still passing PART A
        cout << "PART C:" << endl;
        list<int> myList = { 1, 2, 3, 4 };
        cout << myList << endl;
        std::ostringstream oss;
        oss << myList << " " << myList;
        assert(oss.str() == "1 2 3 4 1 2 3 4");
        cout << myList << endl;
        cout << "passed." << endl;
    }
    {   // PART D: Make your << operator templated so that it can pass
        // the following assertion while still passing PART B
        cout << "PART D:" << endl;
        list<int> list1;
        list<int> list2;
        std::istringstream iss("1 2 3 4 1 2 3 4");
        iss >> list1 >>  list2;
    
        list<int> expectedList1 = { 1, 2, 3, 4, 1, 2, 3, 4 };
        list<int> expectedList2 = { };
        
        assert(list1 == expectedList1);
        assert(list2 == expectedList2);
        cout << "passed." << endl;
    }
    {   // PART E: use file streams instead
	list<int> myList = { 1, 2, 3, 4 };
	ofstream outFile("test.txt", std::ios::out);
	outFile << myList << endl;
	outFile.close();

	ifstream inFile("test.txt");
	list<int> inList;
	inFile >> inList;
	inFile.close();
	assert(myList == inList);

	inFile.open("test.txt");
	list<string> inStrList;
	inFile >> inStrList;
	inFile.close();
	list<string> expected = {"1","2","3","4"};
	assert(inStrList == expected);
        // DO TOGETHER:
    }
    {   // PART F: do some formatted output format some output
        // DO TOGETHER:
    }
    cout << "All tests passed." << endl;
    #if 0 // COMMENTING OUT FROM HERE TO THE END
    #endif // END OF COMMENTED OUT CODE
}

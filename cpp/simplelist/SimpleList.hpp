#include <cstdio>
#include <list>
#include <string>
#include <sstream>
#include <cassert>
#include <iostream>

// forward declarations
template <class T> class List;

/**
 * writes an stl list to a string (as long as the << operator is
 * defined for the type
 */
template <class T> std::string toString(const std::list<T>& inList) {
    std::ostringstream returnStr;
    typedef typename std::list<T>::const_iterator itr_t; // problem
    for (itr_t itr = inList.begin(); itr != inList.end(); ++itr) {
        returnStr << *itr << std::endl;
    }
    return returnStr.str();
}

/**
 * prints out our List
 */
template<class T>
std::ostream& print(const List<T>& a, std::ostream& out = std::cout)  {
    out << a.data << std::endl;
    if (a.tail) {
	// returning out lets me call:
	// print(list2, print(list1, out));
        return print(*(a.tail), out);
    } else {
	return out;
    }
}


template<class T>
std::ostream& operator <<(std::ostream& out, const List<T>& list) {
    return print(list, out);
}


/**
 * simple linked list
 */
template <class T> class List {
    // friend line gives print function access
    // the <> is there since print is a templated function
    friend std::ostream& print<>(const List<T>&, std::ostream&); 
    T data;
    List* tail;

public:
    List(T newData) : data(newData), tail(NULL) {
        
    }

    List& getLast() {
        if (tail) {
            return tail->getLast();
        } else {
            return *this;
        }
    }

    void append(T newData) {
        getLast().tail = new List(newData);
    }

    std::list<T> toSTL() {
	std::list<T> returnList;
        List* cur = this;
        while (cur) {
            returnList.push_back(cur->data);
            cur = cur->tail;
        }
        return returnList;
    }

    /**
     * A simple const iterator for our List
     */
    class Iterator {
        // friend line gives List<T> access to constructor (and cur)
        friend class List<T>; // doesn't compile without this
        const List<T>* cur;
        
        Iterator(List<T>* newCur) : cur(newCur) {

        }
    public:

        // pre increment the iterator
        void operator ++() {
            assert(cur);
            cur = cur->tail;
        }

	// post increment
        void operator ++(int) {
	    ++(*this);
        }
        
        // get the value at the place pointed to by the iterator
        const T& operator *() const {
            assert(cur);
            return cur->data;
        }

        bool operator ==(const List<T>::Iterator& rhs) {
            return cur == rhs.cur;
        }

        bool operator !=(const List<T>::Iterator& rhs) {
            return !(cur == rhs.cur);
        }
        
    };

    // return iterator starting at the front of the list
    Iterator begin() {
        Iterator itr(this);
        return itr;
    }

    // return a fixed iterator that points one past the end
    const Iterator& end() {
        static Iterator itr(NULL);
        return itr;
    }

};


#include "SimpleList.hpp"
#include <sstream>
#include <string>
#include <cassert>
#include <iostream>

using std::string;
using std::ostringstream;
using std::endl;


void printTest() {

    List<string> myList("5");
    myList.append("test");
    myList.append("7");
    string expected = "5\ntest\n7\n";

    {   // via toString
	string s = toString(myList.toSTL());
	assert(s == expected);
    }

    {   // via iterator
	ostringstream out;
	typedef List<string>::Iterator itr_t;
	for (itr_t itr = myList.begin(); itr != myList.end(); ++itr) {
	    out << *itr << endl;
	}
	string found = out.str();
	assert(found == expected);
    }
    
    {   // via print
	ostringstream out;
	print(myList, out);
	string found = out.str();
	assert(found == expected);
    }

    {   // via << operator
	ostringstream out;
	out << myList;
	string found = out.str();
	assert(found == expected);
    }

}


int main() {
    printTest();
}

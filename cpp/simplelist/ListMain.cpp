#include "SimpleList.hpp"
#include <iostream>
#include <sstream>
#include <string>

using std::string;
using std::ostringstream;
using std::cout;
using std::endl;

int main() {
    List<string> myList("5");
    myList.append("test");
    myList.append("7");
    cout << "First Print: " << endl;
    cout << "-------------" << endl;
    cout << toString(myList.toSTL()) << endl;

    cout << "Second Print: " << endl;
    cout << "-------------" << endl;
    typedef List<string>::Iterator itr_t;
    for (itr_t itr = myList.begin(); itr != myList.end(); ++itr) {
        cout << *itr << endl;
    }
    cout << endl;

    cout << "Third Print: " << endl;
    cout << "-------------" << endl;
    print(myList);
    cout << endl;

    cout << "Fourth Print: " << endl;
    cout << "-------------" << endl;
    cout << myList << endl;

    cout << "Fifth Print: " << endl;
    cout << "-------------" << endl;
    ostringstream out;
    out << myList << endl;
    string s = out.str();
    cout << s << endl;
}

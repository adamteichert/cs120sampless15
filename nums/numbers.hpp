#include <string>
#include <sstream>
#include <iostream>

class Number {
public:
    virtual operator std::string() const = 0;
    virtual void operator +=(int i) = 0;
    virtual ~Number() { }
    
};

class Double : public Number {
    double data;
    
public:
    Double(double d) : data(d) {
    }
    operator std::string() const {
	std::ostringstream out;
	out << data;
	return out.str();
    }
    void operator +=(int i) {
	data += i;
    }
};

class Rational : public Number {
    int numer;
    int denom;

protected:
    int getNumer() const { return numer; }

public:
    Rational(int n, int d) : numer(n), denom(d) {
    }
    operator std::string() const {
	std::ostringstream out;
	out << numer << "/" << denom;
	return out.str();
    }

    void operator +=(int i) {
	numer += i * denom;
    }
    
};

class Int : public Rational {

public:
    Int(int data) : Rational(data, 1) {

    }
    operator std::string() const {
	std::ostringstream out;
	out << getNumer();
	return out.str();
    }

    ~Int() {
	std::cout << "calling int destructor" << std::endl;
    }
};


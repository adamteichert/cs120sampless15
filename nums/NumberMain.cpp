#include "numbers.hpp"
#include <vector>
#include <string>
#include <iostream>
#include <cassert>
#include <sstream>

using std::string;
using std::vector;
using std::endl;
using std::ostringstream;


template <class T>
void freevec(vector<T*> v) {
    while (v.size() > 0) {
	T* last = v.back();
	delete last;
	v.pop_back();
    }
}

int main() {
    vector<Number*> nums;
    nums.push_back(new Double(4.5));
    nums.push_back(new Rational(4, 5));
    nums.push_back(new Int(3));
    
    ostringstream out;
    typedef vector<Number*>::iterator itr_t;
    for (itr_t itr = nums.begin(); itr != nums.end(); ++itr) {
	Number& num = **itr;
	num += 10;
	out << (string) num << endl;
    }
    string found = out.str();
    assert(found == "14.5\n54/5\n13\n");
    freevec(nums);

    vector<Rational*> rationals;
    rationals.push_back(new Rational(4, 5));
    rationals.push_back(new Int(3));
    freevec(rationals);
}
